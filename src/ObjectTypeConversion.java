
public class ObjectTypeConversion {
	public static void main (String[] args){
		//converting a primitive to object type
		int number= 100;
		int number4 = 10002;
		Integer obj = Integer.valueOf(number); // converting int into Integer explicitly
		Integer objnum = number4; //implicit type cov -> autoboxing
		System.out.println(number + " "+ obj);
		
		// converting obj type to primitive type
		Integer obj2 = new Integer (100);
		Integer intobj2 =new Integer (500);
		int num = obj2.intValue(); //converting object int into premitive explicitly
		int num3 = intobj2; // converting object into into auto unboxing
		System.out.println(num+" "+obj2);
		
		//converting a primitive to object type
				float number1= 100;
				Float obj1 = Float.valueOf(number1);
				System.out.println(number1 + " "+ obj1);
				
		// converting obj3 type to primitive type
				Float obj3 = new Float (100);
				float num2 = obj3.floatValue();
				System.out.println(num2+" "+obj3);
				
		//converting a primitive to object type
				double number3= 100;
				Double obj4 = Double.valueOf(number3);
				System.out.println(number1 + " "+ obj4);
				
		// converting obj5 type to primitive type
				Double obj5 = new Double (100);
				double num5 = obj5.doubleValue();
				System.out.println(num5+" "+obj5);
	}

}
